import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';

// but this still couples the Heroes component to the actual heroService implementation..
import { HeroService } from '../hero.service';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  heroes: Hero[];
  
  constructor(private heroService: HeroService, private messageService : MessageService) {
    
  }

  ngOnInit() {
    this.getHeroes();
  }

  selectedHero: Hero;
  onSelect(hero: Hero) {
    this.selectedHero = hero;
    this.messageService.add('Chose:' + hero.name);
  }

  getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe(heroes => this.heroes = heroes);
  }
}
