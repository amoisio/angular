import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeroesComponent } from './heroes/heroes.component';

const routes: Routes = [
  { path: 'heroes', component: HeroesComponent }
];

// how to compile the components template and
// how to create an injector at runtime
@NgModule({
  // The exported declarables of the listed modules are available to the template of this module
  imports: [
    RouterModule.forRoot(routes)
  ],
  // There are made public so the user of this component can use them as well
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
